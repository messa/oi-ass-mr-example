#!/bin/bash

set -ex

rm -rf *.class

javac -cp /usr/lib/hadoop-0.20/hadoop-core.jar:/usr/lib/hadoop-0.20/lib/commons-cli-1.2.jar Movies.java

jar cvf Movies.jar *.class

rm -rfv output

hadoop jar Movies.jar Movies input output


ls -l output
cat output/part*


