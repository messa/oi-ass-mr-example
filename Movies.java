
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;


public class Movies {

    /*
     * Radky "<nodeAddress> <movieHash>" prevede na (movieHash, nodeAddress)
     */
    public static class MovieNodeMapper
        extends Mapper<Object, Text, Text, Text> {

        public void map(Object key,
                        Text value,
                        Context context)
            throws IOException, InterruptedException
        {
            String line = value.toString();
            if (line.equals("") || line.startsWith("#")) {
                return;
            }
            StringTokenizer st = new StringTokenizer(line);
            if (st.countTokens() != 2) {
                throw new RuntimeException("Invalid line: " + line);
            }
            String nodeAddress = st.nextToken();
            String movieHash = st.nextToken();
            // zde by byla nejaka kontrola na to geoip nodu...
            context.write(new Text(movieHash), new Text(nodeAddress));
        }
    }

    /*
     * Z mapperu dostane (movieHash, nodeAddress), zapise nazev filmu a
     * pocet unikatnich nodeAddress: (count, movieName)
     */
    public static class MovieCountReducer
        extends Reducer<Text, Text, IntWritable, Text> {

        public void reduce(Text key,
                           Iterable<Text> values,
                           Context context)
            throws IOException, InterruptedException
        {
            String movieHash = key.toString();
            String movieName = movieNamesByHash.get(movieHash);
            Set<String> nodeAddresses = new HashSet<String>();
            int count = 0;
            for (Text value : values) {
                String nodeAddress = value.toString();
                if (nodeAddresses.contains(nodeAddress)) {
                    continue;
                }
                nodeAddresses.add(nodeAddress);
                count ++;
            }
            context.write(new IntWritable(count), new Text(movieName));
        }
    }

    private static Map<String, String> movieNamesByHash = new HashMap<String, String>();

    public static void main(String[] args) throws Exception {
        // tohle by se normalne nacitalo z nejake databaze:
        movieNamesByHash.put("abcdef", "Terminator 1");
        movieNamesByHash.put("abc123", "Terminator 2");
        movieNamesByHash.put("481aff", "Terminator 3");
        movieNamesByHash.put("cafeff", "Alien");

        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        if (otherArgs.length != 2) {
            System.err.println("Usage: movies <in> <out>");
            System.exit(2);
        }
        Job job = new Job(conf, "movies");
        job.setJarByClass(Movies.class);
        job.setMapperClass(MovieNodeMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setReducerClass(MovieCountReducer.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
        FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}

